﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Model
{
    public class Student: Logger
    {
        public Student()
        {

        }
        [Key]
        public int Id { get; set; }

        [MaxLength(255)]
        [MinLength(1)]
        [Required]
        public string FirstName { get; set; }

        [MaxLength(255)]
        [MinLength(1)]
        [Required]
        public string LastName { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}