﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AspNetWebApi.Controllers;
using System.Web.Http.Results;
using Model;
using BLL;
using System.Data.Entity;
using DAL;
using DAL.Tests;
using System.Web.Mvc;
using Model.Enumerations;
using System.Web;
using System.Web.Http;

namespace AspNetWebApi.Tests.Controllers
{
    [TestClass]
    public class StudentsControllerTest
    {
        IStudentRepositoy _studentRepository;
        IWebApiAngularContext _webApiAngularContext;
        IStudentService _studentService;
        
        public StudentsControllerTest()
        {

        }
        [TestMethod]
        public void GetOrderedByFirstName()
        {

            // Arrange
            IWebApiAngularContext _webApiAngularContext = new TestWebApiAngularContext();
            _webApiAngularContext.Students.Add(new Student {Id=1, FirstName = "Mike", LastName = "McDonald", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 2, FirstName = "Marc", LastName = "Rouin", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 3, FirstName = "Tim", LastName = "Lepp", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id =4, FirstName = "Steen", LastName = "Jensen", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 5, FirstName = "AAAA", LastName = "ZZZZ", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 6, FirstName = "ZZZZ", LastName = "AAAA", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 7, FirstName = "Alex", LastName = "Wood", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 8, FirstName = "Therese", LastName = "Gibson", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 9, FirstName = "Marina", LastName = "Julien", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 10, FirstName = "Ray", LastName = "OBrian", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 11, FirstName = "Brent", LastName = "Dumont", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 12, FirstName = "Keegan", LastName = "Fortin", CreatedDate = DateTime.Now });

            IStudentRepositoy _studentRepository = new StudentRepositoy(_webApiAngularContext);
            IStudentService _studentService = new StudentService(_studentRepository);
            StudentsController controller = new StudentsController(_studentService);

            // Act
            var result = controller.GetStudents((int)OrderByFieldEnum.FirstName).Result as OkNegotiatedContentResult<List<Student>>; ;
            // Assert
            Assert.IsNotNull(result.Content);
            Assert.AreEqual(_webApiAngularContext.Students.Count(), result.Content.Count());
            Assert.IsTrue(result.Content.First().FirstName == "AAAA");
            Assert.IsTrue(result.Content.Last().FirstName == "ZZZZ");
        }

        [TestMethod]
        public void GetOrderedByLastName()
        {

            // Arrange
            IWebApiAngularContext _webApiAngularContext = new TestWebApiAngularContext();
            _webApiAngularContext.Students.Add(new Student { Id = 1, FirstName = "Mike", LastName = "McDonald", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 2, FirstName = "Marc", LastName = "Rouin", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 3, FirstName = "Tim", LastName = "Lepp", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 4, FirstName = "Steen", LastName = "Jensen", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 5, FirstName = "AAAA", LastName = "ZZZZ", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 6, FirstName = "ZZZZ", LastName = "AAAA", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 7, FirstName = "Alex", LastName = "Wood", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 8, FirstName = "Therese", LastName = "Gibson", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 9, FirstName = "Marina", LastName = "Julien", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 10, FirstName = "Ray", LastName = "OBrian", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 11, FirstName = "Brent", LastName = "Dumont", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 12, FirstName = "Keegan", LastName = "Fortin", CreatedDate = DateTime.Now });

            IStudentRepositoy _studentRepository = new StudentRepositoy(_webApiAngularContext);
            IStudentService _studentService = new StudentService(_studentRepository);
            StudentsController controller = new StudentsController(_studentService);

            // Act
            var result = controller.GetStudents((int)OrderByFieldEnum.LastName).Result as OkNegotiatedContentResult<List<Student>>; ;
            // Assert
            Assert.IsNotNull(result.Content);
            Assert.AreEqual(_webApiAngularContext.Students.Count(), result.Content.Count());
            Assert.IsTrue(result.Content.First().LastName == "AAAA");
            Assert.IsTrue(result.Content.Last().LastName == "ZZZZ");
        }


        [TestMethod]
        public void GetBadRequestOnGetStudentsWithoutOrderByParameter()
        {
            // Arrange
            IStudentRepositoy _studentRepository = new StudentRepositoy(_webApiAngularContext);
            IStudentService _studentService = new StudentService(_studentRepository);
            StudentsController controller = new StudentsController(_studentService);

            // Act
            var result = controller.GetStudents(null).Result;// as ActionResult; 

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(BadRequestErrorMessageResult));
            Assert.AreEqual((result as BadRequestErrorMessageResult).Message, "orderBy parameter is required");
        }

        [TestMethod]
        public void GetStudentById()
        {
            Student student = new Student { Id = 4, FirstName = "Steen", LastName = "Jensen", CreatedDate = DateTime.Now };
            // Arrange
            IWebApiAngularContext _webApiAngularContext = new TestWebApiAngularContext();
            _webApiAngularContext.Students.Add(new Student { Id = 1, FirstName = "Mike", LastName = "McDonald", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 2, FirstName = "Marc", LastName = "Rouin", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 3, FirstName = "Tim", LastName = "Lepp", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(student);
            _webApiAngularContext.Students.Add(new Student { Id = 5, FirstName = "AAAA", LastName = "ZZZZ", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 6, FirstName = "ZZZZ", LastName = "AAAA", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 7, FirstName = "Alex", LastName = "Wood", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 8, FirstName = "Therese", LastName = "Gibson", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 9, FirstName = "Marina", LastName = "Julien", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 10, FirstName = "Ray", LastName = "OBrian", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 11, FirstName = "Brent", LastName = "Dumont", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 12, FirstName = "Keegan", LastName = "Fortin", CreatedDate = DateTime.Now });
            IStudentRepositoy _studentRepository = new StudentRepositoy(_webApiAngularContext);
            IStudentService _studentService = new StudentService(_studentRepository);
            StudentsController controller = new StudentsController(_studentService);

            // Act
            var result = controller.GetStudent(student.Id).Result as OkNegotiatedContentResult<Student>; ;
            // Assert
            Assert.IsNotNull(result.Content);
            Assert.IsInstanceOfType(result.Content, typeof(Student));
            Assert.AreEqual(student, result.Content);
            Assert.AreSame(student, result.Content);
        }

        [TestMethod]
        public void GetNotFoundOnGetStudentByIdWithNonExistingId()
        {
            Student student = new Student { Id = 4, FirstName = "Steen", LastName = "Jensen", CreatedDate = DateTime.Now };
            // Arrange
            IWebApiAngularContext _webApiAngularContext = new TestWebApiAngularContext();
            _webApiAngularContext.Students.Add(new Student { Id = 1, FirstName = "Mike", LastName = "McDonald", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 2, FirstName = "Marc", LastName = "Rouin", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 3, FirstName = "Tim", LastName = "Lepp", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(student);
            _webApiAngularContext.Students.Add(new Student { Id = 5, FirstName = "AAAA", LastName = "ZZZZ", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 6, FirstName = "ZZZZ", LastName = "AAAA", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 7, FirstName = "Alex", LastName = "Wood", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 8, FirstName = "Therese", LastName = "Gibson", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 9, FirstName = "Marina", LastName = "Julien", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 10, FirstName = "Ray", LastName = "OBrian", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 11, FirstName = "Brent", LastName = "Dumont", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 12, FirstName = "Keegan", LastName = "Fortin", CreatedDate = DateTime.Now });
            IStudentRepositoy _studentRepository = new StudentRepositoy(_webApiAngularContext);
            IStudentService _studentService = new StudentService(_studentRepository);
            StudentsController controller = new StudentsController(_studentService);

            // Act
            var result = controller.GetStudent(99).Result;
            // Assert
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        [TestMethod]
        public void CreateNewStudent()
        {

            // Arrange
            Student newStudent = new Student { FirstName = "New", LastName = "Guy" };

            IWebApiAngularContext _webApiAngularContext = new TestWebApiAngularContext();
            IStudentRepositoy _studentRepository = new StudentRepositoy(_webApiAngularContext);
            IStudentService _studentService = new StudentService(_studentRepository);
            StudentsController controller = new StudentsController(_studentService);

            // Act
            var result = controller.PostStudent(newStudent).Result as CreatedAtRouteNegotiatedContentResult<Student>; ;
            // Assert
            Assert.IsNotNull(result.Content);
            Assert.AreEqual(1, (_webApiAngularContext as TestWebApiAngularContext).SaveChangesCount);
            Assert.AreEqual(1, _webApiAngularContext.Students.Count());
            Assert.IsInstanceOfType(result.Content, typeof(Student));
            Assert.AreNotEqual(DateTime.MinValue, result.Content.CreatedDate);
            Assert.AreEqual(DateTime.MinValue, result.Content.AddedDate);
        }

        [TestMethod]
        public void GetBadRequestOnCreateNewStudentWithEmptyFirstName()
        {

            // Arrange
            Student newStudent = new Student { FirstName = "", LastName = "Guy" };

            IWebApiAngularContext _webApiAngularContext = new TestWebApiAngularContext();

            IStudentRepositoy _studentRepository = new StudentRepositoy(_webApiAngularContext);
            IStudentService _studentService = new StudentService(_studentRepository);
            StudentsController controller = new StudentsController(_studentService);
            controller.ModelState.AddModelError("student.FirstName", "The field FirstName must be a string or array type with a minimum length of '1'.");
            controller.ModelState.AddModelError("student.FirstName", "The FirstName field is required.");

            // Act
            var result = controller.PostStudent(newStudent).Result;
            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(0, (_webApiAngularContext as TestWebApiAngularContext).SaveChangesCount);
            Assert.AreEqual(0, _webApiAngularContext.Students.Count());
            Assert.IsInstanceOfType(result, typeof(InvalidModelStateResult));

        }

        [TestMethod]
        public void UdpadeStudent()
        {

            // Arrange
            Student student = new Student { Id = 4, FirstName = "Steen", LastName = "Jensen", CreatedDate = DateTime.Now };

            IWebApiAngularContext _webApiAngularContext = new TestWebApiAngularContext();
            _webApiAngularContext.Students.Add(new Student { Id = 1, FirstName = "Mike", LastName = "McDonald", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 2, FirstName = "Marc", LastName = "Rouin", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 3, FirstName = "Tim", LastName = "Lepp", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(student);
            _webApiAngularContext.Students.Add(new Student { Id = 5, FirstName = "AAAA", LastName = "ZZZZ", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 6, FirstName = "ZZZZ", LastName = "AAAA", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 7, FirstName = "Alex", LastName = "Wood", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 8, FirstName = "Therese", LastName = "Gibson", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 9, FirstName = "Marina", LastName = "Julien", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 10, FirstName = "Ray", LastName = "OBrian", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 11, FirstName = "Brent", LastName = "Dumont", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 12, FirstName = "Keegan", LastName = "Fortin", CreatedDate = DateTime.Now });

            IStudentRepositoy _studentRepository = new StudentRepositoy(_webApiAngularContext);
            IStudentService _studentService = new StudentService(_studentRepository);
            StudentsController controller = new StudentsController(_studentService);
            student.LastName = "Donky";
            student.FirstName = "Ass";
            // Act
            var result = controller.PutStudent(student.Id, student).Result as StatusCodeResult; ;
            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(System.Net.HttpStatusCode.NoContent, result.StatusCode);
            Assert.AreEqual(1, (_webApiAngularContext as TestWebApiAngularContext).SaveChangesCount);
            Assert.AreEqual(12, _webApiAngularContext.Students.Count());
            Assert.AreEqual(student, _webApiAngularContext.Students.FirstOrDefault(s => s.Id == student.Id));
            Assert.AreEqual("Donky", _webApiAngularContext.Students.FirstOrDefault(s => s.Id == student.Id).LastName);
            Assert.AreEqual("Ass", _webApiAngularContext.Students.FirstOrDefault(s => s.Id == student.Id).FirstName);
        }

        [TestMethod]
        public void GetBadRequestOnUpdateStudentWithEmptyFirstName()
        {

            // Arrange
            Student student = new Student { Id = 4, FirstName = "Steen", LastName = "Jensen", CreatedDate = DateTime.Now };

            IWebApiAngularContext _webApiAngularContext = new TestWebApiAngularContext();
            _webApiAngularContext.Students.Add(new Student { Id = 1, FirstName = "Mike", LastName = "McDonald", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 2, FirstName = "Marc", LastName = "Rouin", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 3, FirstName = "Tim", LastName = "Lepp", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(student);
            _webApiAngularContext.Students.Add(new Student { Id = 5, FirstName = "AAAA", LastName = "ZZZZ", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 6, FirstName = "ZZZZ", LastName = "AAAA", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 7, FirstName = "Alex", LastName = "Wood", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 8, FirstName = "Therese", LastName = "Gibson", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 9, FirstName = "Marina", LastName = "Julien", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 10, FirstName = "Ray", LastName = "OBrian", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 11, FirstName = "Brent", LastName = "Dumont", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 12, FirstName = "Keegan", LastName = "Fortin", CreatedDate = DateTime.Now });


            IStudentRepositoy _studentRepository = new StudentRepositoy(_webApiAngularContext);
            IStudentService _studentService = new StudentService(_studentRepository);
            StudentsController controller = new StudentsController(_studentService);
            controller.ModelState.AddModelError("student.FirstName", "The field FirstName must be a string or array type with a minimum length of '1'.");
            controller.ModelState.AddModelError("student.FirstName", "The FirstName field is required.");
            student.LastName = "Donky";
            student.FirstName = "";
            // Act
            var result = controller.PutStudent(student.Id, student).Result;
            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(0, (_webApiAngularContext as TestWebApiAngularContext).SaveChangesCount);
            Assert.AreEqual(12, _webApiAngularContext.Students.Count());
            Assert.IsInstanceOfType(result, typeof(InvalidModelStateResult));

        }

        [TestMethod]
        public void GetNotFoundOnUpdateStudentWithNonExistingId()
        {

            // Arrange
            Student student = new Student { Id = 4, FirstName = "Steen", LastName = "Jensen", CreatedDate = DateTime.Now };

            IWebApiAngularContext _webApiAngularContext = new TestWebApiAngularContext();
           

            IStudentRepositoy _studentRepository = new StudentRepositoy(_webApiAngularContext);
            IStudentService _studentService = new StudentService(_studentRepository);
            StudentsController controller = new StudentsController(_studentService);
             student.Id = 99;
            // Act
            var result = controller.PutStudent(student.Id, student).Result;
            // Assert
            Assert.IsNotNull(result);
           // Assert.AreEqual(System.Net.HttpStatusCode.NoContent, result.StatusCode);
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
            Assert.AreEqual(0, (_webApiAngularContext as TestWebApiAngularContext).SaveChangesCount);
            Assert.AreEqual(0, _webApiAngularContext.Students.Count());

        }

        [TestMethod]
        public void DeleteStudent()
        {

            // Arrange
            Student student = new Student { Id = 4, FirstName = "Steen", LastName = "Jensen", CreatedDate = DateTime.Now };

            IWebApiAngularContext _webApiAngularContext = new TestWebApiAngularContext();
            _webApiAngularContext.Students.Add(new Student { Id = 1, FirstName = "Mike", LastName = "McDonald", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 2, FirstName = "Marc", LastName = "Rouin", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 3, FirstName = "Tim", LastName = "Lepp", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(student);
            _webApiAngularContext.Students.Add(new Student { Id = 5, FirstName = "AAAA", LastName = "ZZZZ", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 6, FirstName = "ZZZZ", LastName = "AAAA", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 7, FirstName = "Alex", LastName = "Wood", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 8, FirstName = "Therese", LastName = "Gibson", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 9, FirstName = "Marina", LastName = "Julien", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 10, FirstName = "Ray", LastName = "OBrian", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 11, FirstName = "Brent", LastName = "Dumont", CreatedDate = DateTime.Now });
            _webApiAngularContext.Students.Add(new Student { Id = 12, FirstName = "Keegan", LastName = "Fortin", CreatedDate = DateTime.Now });

            IStudentRepositoy _studentRepository = new StudentRepositoy(_webApiAngularContext);
            IStudentService _studentService = new StudentService(_studentRepository);
            StudentsController controller = new StudentsController(_studentService);
             // Act
            var result = controller.DeleteStudent(student.Id).Result as OkNegotiatedContentResult<Student>; ;
            // Assert
             Assert.IsNotNull(result.Content);
            Assert.IsInstanceOfType(result.Content, typeof(Student));
            Assert.IsNotNull(result);
            Assert.AreEqual(1, (_webApiAngularContext as TestWebApiAngularContext).SaveChangesCount);
            Assert.AreEqual(11, _webApiAngularContext.Students.Count());
            Assert.AreNotEqual(student, _webApiAngularContext.Students.FirstOrDefault(s => s.Id == student.Id));
       }

        [TestMethod]
        public void GetNotFoundOnDeleteStudentWithNonExistingId()
        {

            // Arrange
            Student student = new Student { Id = 4, FirstName = "Steen", LastName = "Jensen", CreatedDate = DateTime.Now };

            IWebApiAngularContext _webApiAngularContext = new TestWebApiAngularContext();
            
            IStudentRepositoy _studentRepository = new StudentRepositoy(_webApiAngularContext);
            IStudentService _studentService = new StudentService(_studentRepository);
            StudentsController controller = new StudentsController(_studentService);
            student.Id = 99;
            // Act
            var result = controller.DeleteStudent(student.Id).Result;
            // Assert
            Assert.IsNotNull(result);
            // Assert.AreEqual(System.Net.HttpStatusCode.NoContent, result.StatusCode);
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
            Assert.AreEqual(0, (_webApiAngularContext as TestWebApiAngularContext).SaveChangesCount);
            Assert.AreEqual(0, _webApiAngularContext.Students.Count());

        }


    }



}
