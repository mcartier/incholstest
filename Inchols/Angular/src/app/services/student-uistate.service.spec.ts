import { TestBed } from '@angular/core/testing';

import { StudentUIStateService } from './student-uistate.service';

describe('StudentUIStateService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StudentUIStateService = TestBed.get(StudentUIStateService);
    expect(service).toBeTruthy();
  });
});
