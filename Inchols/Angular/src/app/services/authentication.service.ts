import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { StateService } from '@uirouter/core';
import { map } from 'rxjs/operators';
import { BehaviorSubject } from "rxjs";
import { Observable, of } from 'rxjs';
import { AlertService } from "./alert.service.js";
@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    constructor(private alertService:AlertService,private http: HttpClient, private states: StateService) { }
    private _logedInStateObs: BehaviorSubject<boolean> = new BehaviorSubject(this.checkAuthenticated());
    public readonly LogedInStateObs: Observable<boolean> = this._logedInStateObs.asObservable();

     register(username: string, password: string, confirmpassword:string, email: string) {
         const httpOptions = {
             headers: new HttpHeaders({
                 'Content-Type': 'application/json'
             })
         };
         let data = {
             username: username,
             password: password,
             confirmpassword: confirmpassword,
             email: email
         };
          return this.http.post<any>(
              "/api/account/register",
              data,
              httpOptions
        ).toPromise()
              .then(() => {
                  this.alertService.success("User " + username + " successfuly registered")
                return this.login(username,password);
            }).catch((error) => {
                this._logedInStateObs.next(false);
                localStorage.removeItem('currentUser');
                this.alertService.error(error, "Registration for user " + username + " failed");
            })

    }


    login(username: string, password: string) {

        return this.http.post<any>("/oauth2/token", `username=${username}&password=${password}&grant_type=password&client_id=099153c2625149bc8ecb3e85e03f0022`
         ).toPromise()
            .then((user) => {
                if (user && user.access_token) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
                    this._logedInStateObs.next(true);
                }
            }).catch((error) => {
                this._logedInStateObs.next(false);
                localStorage.removeItem('currentUser');
                this.alertService.error(error, "Login for " + username + " failed", [(error.error.error_description) ? error.error.error_description : error.error.error], false);

            })

    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        this._logedInStateObs.next(false);

        //const $state = this.trans.router.stateService;
        this.states.go('home', undefined, undefined);

    }

    checkAuthenticated() {

        return !(localStorage.getItem("currentUser") === null)
    }
}