import { TestBed } from '@angular/core/testing';

import { StudentBackendService } from './student-backend.service';

describe('StudentBackendService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StudentBackendService = TestBed.get(StudentBackendService);
    expect(service).toBeTruthy();
  });
});
