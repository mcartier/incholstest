import { Injectable, Inject } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map, retry, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Student } from '../../models/student';
import { WINDOW } from '../browser.window.provider';
import { HttpHelpers } from '../../utilities/HttpHelpers';
@Injectable({
  providedIn: 'root'
})
export class StudentBackendService extends HttpHelpers {

    constructor(private http: HttpClient, @Inject(WINDOW) private window: Window) {
        super(http);
        this.host = window.location.host;
        this.hostName = window.location.hostname;
        this.protocol = window.location.protocol;
        this.port = window.location.port;

    }
    private host: string;
    private hostName: string;
    private protocol: string;
    private port: string;
    private getBaseURL(): string {
        // return this.protocol + "://" + this.hostName + ":" + this.port + "/api/students/";
        return "/api/students/";
    }

    public createStudent(student: Student): Observable<Student> {
        let controllerQuery = this.getBaseURL();
        return this.postAction<Student>(student, controllerQuery)
            .pipe(
                map((res: Student) => {
                    //do anything with the returned data here...
                    return res
                })
            )
    };

    public saveStudent(student: Student): Observable<Student> {
        let controllerQuery = this.getBaseURL() + student.id;
        return this.putAction<Student>(student, controllerQuery)
            .pipe(
                map((res: Student) => {
                    //do anything with the returned data here...
                    return res
                })
            )
    };


    public deleteStudent(studentId: number): Observable<Student> {
        let controllerQuery = this.getBaseURL() + studentId;
        return this.deleteAction<Student>(controllerQuery)
            .pipe(
            map((res: Student) => {
                    //do anything with the returned data here...
                    return res
                })
            )
    };



    public getStudents(orderByIndex: number): Observable<Student[]> {
        let controllerQuery = this.getBaseURL() + "?orderBy=" + orderByIndex;
          return this.getAction<Student[]>(controllerQuery)
            .pipe(
                map((res: Student[]) => {
                    //do anything with the returned data here...
                    return res
                })
            );
    };

    public getStudent(studentId: number): Observable<Student> {
        let controllerQuery = this.getBaseURL() + studentId;
        return this.getAction<Student>(controllerQuery)
            .pipe(
                map((res: Student) => {
                    //do anything with the returned data here...
                    return res
                })
            );
    };

}
