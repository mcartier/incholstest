import { Injectable, Inject } from '@angular/core';
import { Observable, of } from 'rxjs';
import {  HttpErrorResponse } from '@angular/common/http';
import { map, retry, catchError } from 'rxjs/operators';
import { Student } from '../models/student';
import { StudentBackendService } from './backend/student-backend.service';
import { Subject } from "rxjs";
import { BehaviorSubject, throwError } from "rxjs";
import { AlertService } from "./alert.service.js";
import { IAlert, AlertType } from '../models/alertInterface'
@Injectable({
    providedIn: 'root'
})
export class StudentService {

    constructor(private alertService:AlertService,private backEnd: StudentBackendService) {


    }

    private compareFirstName(a, b): number {
        if (a.firstName < b.firstName)
            return -1;
        if (a.firstName > b.firstName)
            return 1;
        return 0;
    };
    private compareLastName(a, b): number {
        if (a.lastName < b.lastName)
            return -1;
        if (a.lastName > b.lastName)
            return 1;
        return 0;
    };
    nyteest() {

        return () => { }
    }
    private _students: Student[] = [];
    private _orderByIndex: number = 0;
    private _studentsObs: BehaviorSubject<Student[]> = new BehaviorSubject([]);
    private _studentObsArray = {};
    public readonly studentsObs: Observable<Student[]> = this._studentsObs.asObservable();

    public SubscribeToStudentObs(studentId: number): Observable<Student> {

        return this._getOrCreateStudentObsInArray(studentId).asObservable();
    }

    private makeString(value: number): string {
        return value.toString();
    };
    private _getOrCreateStudentObsInArray(studentId: number): BehaviorSubject<Student> {
        var studentId$ = this.makeString(studentId);
        if (!(studentId$ in this._studentObsArray)) {
            this._studentObsArray[studentId$] = new BehaviorSubject<Student>({
                firstName: "",
                lastName: "",
                id: 0,
                createdDate: new Date
            });
        }
        return this._studentObsArray[studentId$];
    }
    private _removeStudentObsFromArray(studentId: number): void {
        var studentId$ = this.makeString(studentId);
        if (studentId$ in this._studentObsArray) {
            delete this._studentObsArray[studentId$];
        }
    }

    private _isStudentIsStudentArray(studentId: number): boolean {
        return this._students.some(s => s.id == studentId);
    }

    private _getStudents(orderByIndex: number, dispatchAlert: boolean = false): Observable<Student[]> {

        if (orderByIndex == null || orderByIndex != 1) {
            orderByIndex = 0;
        }
        if (this._orderByIndex == orderByIndex && this._students !== null && this._students.length > 0) {
            return of(this._students)
                .pipe(
                    map((res: Student[]) => {
                        return res
                    })
                );
        }
        else {
            return this.backEnd.getStudents(orderByIndex)
                .pipe(
                    map((res: Student[]) => {
                    this._students = res;
                    res.forEach((student) => {
                        if (!this._isStudentIsStudentArray(student.id)) {
                            this._students.push(student);
                        }
                        else {
                            this._students[this._students.findIndex(s => s.id == student.id)] = student;
                        }
                    });
                    if (dispatchAlert) { this.alertService.success("Retrieving students succeeded."); }

                    return res
                }),
                catchError((error) => {
                    return this._handleError(error, "Retrieving students failed.", dispatchAlert);;
                })
                );
        }
    };
    private _getStudent(studentId: number, dispatchAlert: boolean = false): Observable<Student> {

        if (this._isStudentIsStudentArray(studentId)) {
            return of(this._students.find(s => s.id == studentId))
                .pipe(
                    map((res: Student) => {
                        return res
                    })
                );
        }
        else {
            return this.backEnd.getStudent(studentId)
                .pipe(
                map((res: Student) => {
                    if (dispatchAlert) { this.alertService.success("Retrieving student succeeded."); }
                     if (!this._isStudentIsStudentArray(studentId)) {
                         this._students.push(res)
                         this._studentsObs.next(this._students);
                     }
                    
                    return res
                }),
                catchError((error) => {
                    return this._handleError(error, "Retrieving student failed.", dispatchAlert);;
                })
                );
        }
    };
     public LoadStudent(studentId: number, dispatchAlert:boolean = false): void {
        let studObs = this.getStudent(studentId, dispatchAlert);
        studObs.subscribe();
    }
    public LoadStudentList(orderByIndex: number, dispatchAlert: boolean = false): void {
        let studObs = this.getStudents(orderByIndex, dispatchAlert);
        studObs.subscribe();
    }
    public addStudent(student: Student, dispatchAlert: boolean = false): void {
        let studObs = this.createStudent(student, dispatchAlert);
        studObs.subscribe();
    }
    public updateStudent(student: Student, dispatchAlert: boolean = false): void {
        let studObs = this.saveStudent(student, dispatchAlert);
        studObs.subscribe();
    }
    public removeStudent(student: Student, dispatchAlert: boolean = false): void {
        this.removeStudentById(student.id, dispatchAlert);
    }
    public removeStudentById(studentId: number, dispatchAlert: boolean = false): void {
        let studObs = this.deleteStudent(studentId, dispatchAlert);
        studObs.subscribe();
    }
    public getStudents(orderByIndex: number, dispatchAlert: boolean = false): Observable<Student[]> {

        if (orderByIndex == null || orderByIndex != 1) {
            orderByIndex = 0;
        }
        return this._getStudents(orderByIndex, dispatchAlert)
            .pipe(
            map((res: Student[]) => {
                this._studentsObs.next(this._students);

                    return res
            })
            );
    };
    public getStudent(studentId: number, dispatchAlert: boolean = false): Observable<Student> {

        return this._getStudent(studentId, dispatchAlert)
            .pipe(
                map((res: Student) => {
                this._getOrCreateStudentObsInArray(studentId).next(res);
                return res
            })
            );
    };

    public createStudent(student: Student, dispatchAlert: boolean = false): Observable<Student> {
        return this.backEnd.createStudent(student)
            .pipe(
                map((res: Student) => {
                    //do anything with the returned data here...
                if (!this._isStudentIsStudentArray(res.id)) {
                    this._students.push(res);
                    this._studentsObs.next(this._students);
                };
                    
                    this._getOrCreateStudentObsInArray(res.id).next(res);
                this._studentsObs.next(this._students);
                if (dispatchAlert) { this.alertService.success("Saving new student " + student.firstName + " " + student.lastName + " succeeded."); }

                    return res
            }),
            catchError((error) => {
                return this._handleError(error, "Saving new student " + student.firstName + " " + student.lastName + " failed.", dispatchAlert);;
            })
            )
    };



    public saveStudent(student: Student, dispatchAlert: boolean = false): Observable<Student> {

        return this.backEnd.saveStudent(student)
            .pipe(
                map((res: Student) => {
                    //do anything with the returned data here...
                if (!this._isStudentIsStudentArray(student.id)) {
                    this._students.push(student);
                }
                else {
                    this._students[this._students.findIndex(s => s.id == student.id)] = student;
                }
                this._studentsObs.next(this._students);
                this._getOrCreateStudentObsInArray(student.id).next(student);
               if (dispatchAlert) { this.alertService.success("Updating student " + student.firstName + " " + student.lastName + " succeeded."); }

                return student;
            }),
            catchError((error) => {
                return this._handleError(error, "Updating student " + student.firstName + " " + student.lastName + " failed.", dispatchAlert);;
            })
            )
    };

    public deleteStudent(studentId: number, dispatchAlert: boolean = false): Observable<Student> {
        studentId = 99;
        return this.backEnd.deleteStudent(studentId)
            .pipe(
            map((res: Student) => {
                    //do anything with the returned data here...
                    this._students.splice(this._students.findIndex(s => s.id == res.id), 1);
                    this._getOrCreateStudentObsInArray(studentId).complete();
                    this._removeStudentObsFromArray(studentId);
                this._studentsObs.next(this._students);

                    if(dispatchAlert) {this.alertService.success("Deleting student succeeded.");}
                    return res
            }),
            catchError((error) => {
                return this._handleError(error, "Deleting of student failed.", dispatchAlert);;
            })
            )
    };
    private _handleSuccessError(message: string, dispatchAlert: boolean) {
        if (dispatchAlert) {
            this.alertService.success(message);
        }
    }
    private _handleError(error: HttpErrorResponse, message: string, dispatchAlert:boolean) {
        this.alertService.error(error, message);
     
        return throwError(error);
    }

}
