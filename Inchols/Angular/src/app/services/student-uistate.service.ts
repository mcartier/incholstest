import { Injectable } from '@angular/core';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { TransitionService, Transition } from '@uirouter/angular';

@Injectable({
  providedIn: 'root'
})
export class StudentUIStateService {

    constructor(private transitionService: TransitionService) {
        transitionService.onSuccess({}, (trans: Transition) => {
            switch (trans.to().name) {
                case "students":
                    {
                        this._unSetEditMode();
                        break;
                    }
                case "viewstudent":
                    {
                        this._unSetEditMode();
                        break;
                    }
                case "editstudent":
                    {
                        this._setEditMode();
                        break;
                    }
                case "newstudent":
                    {
                        this._setEditMode();
                        break;
                    }
            }

        });
       
    }
    init() {
        // Startup logic here
    }
    private _setEditMode() {
        this._isInEditMode = true;
        this._studentIsInEditModeObs.next(this._isInEditMode);
    }
    private _unSetEditMode() {
        this._isInEditMode = false;
        this._studentIsInEditModeObs.next(this._isInEditMode);

    }
    private _isInEditMode: boolean = false;
    private _studentIsInEditModeObs: BehaviorSubject<boolean> = new BehaviorSubject(false);
    public readonly StudentIsInEditModeObs: Observable<boolean> = this._studentIsInEditModeObs.asObservable();


}
