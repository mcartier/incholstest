import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { UIRouterModule } from "@uirouter/angular";
import { Transition } from "@uirouter/angular"
import { UIRouter } from '@uirouter/angular';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { WINDOW_PROVIDERS } from './services/browser.window.provider';
import { Visualizer } from '@uirouter/visualizer';
import { APP_INITIALIZER } from '@angular/core';
import { ErrorInterceptor } from './interceptors/errorInterceptor';
import { JwtInterceptor } from './interceptors/jwtInterceptor';

import { AppComponent } from './components/app.component';
import { StudentComponent } from './components/students/student/student.component';
import { StudentsComponent } from './components/students/students/students.component';
import { StudentListItemComponent } from './components/students/student-list-item/student-list-item.component';
import { EditStudentComponent } from './components/students/edit-student/edit-student.component';
import { ViewStudentComponent } from './components/students/view-student/view-student.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';

import { UserService } from "./services/user.service.js";
import { StudentUIStateService } from "./services/student-uistate.service.js";
import { AuthenticationService } from "./services/authentication.service.js";
import { StudentService } from "./services/student.service.js";

import { Student } from './models/student';
import { ErrorStateMatcher } from '@angular/material';
import { User } from './models/user';

import { APP_STATES } from './router/ui-router.states';
import { routerConfigFn } from './router/router.config';
import { RegisterComponent } from './components/register/register.component';
//import { NoopAnimationsModule } from '@angular/platform-browser/animations';
//import { PasswordComfirmationErrorStateMatcher } from './components/register/passwordConfirmationErrorStateMatcher'
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { AlertComponent } from './components/alert/alert.component';
import { StudentsContainerComponent } from './components/students/students-container/students-container.component';
function requireAuthentication(transition) {
    // let $state = transition.router.stateService;
    //let authSvc = transition.injector().get(AuthService);

    //  return authSvc.checkAuthenticated().catch(() => $state.target('login'));
}



@NgModule({
    declarations: [
        AppComponent,
        StudentComponent,
        StudentsComponent,
        StudentListItemComponent,
        EditStudentComponent,
        ViewStudentComponent,
        LoginComponent,
        HomeComponent,
        RegisterComponent,
        AlertComponent,
        StudentsContainerComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        ReactiveFormsModule,
        UIRouterModule.forRoot({
            states: APP_STATES,
            useHash: true,
            otherwise: { state: 'home' },
            config: routerConfigFn,
        }),
        FormsModule,
        RxReactiveFormsModule,
       // NoopAnimationsModule
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
       // { provide: ErrorStateMatcher, useClass: PasswordComfirmationErrorStateMatcher },
        WINDOW_PROVIDERS,
        [
            StudentUIStateService,
            {
                provide: APP_INITIALIZER,
                useFactory: (ds: StudentUIStateService) => function () { return ds.init() },
                deps: [StudentUIStateService],
                multi: true
            }]
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
     
}
