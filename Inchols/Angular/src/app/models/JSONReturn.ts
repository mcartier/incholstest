﻿export interface JSONReturnVM<T> {
    element: T;
    errormessage: string;
    haserror: boolean;
}