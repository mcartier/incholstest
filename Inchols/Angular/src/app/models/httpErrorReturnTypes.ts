﻿export class FriendlyHttpErrorReturnObject {
    status: number;
    message: string;
    responseMessage: string;
    fieldValidationMessages: string[];
}