﻿export  interface IAlert {
    alertType: AlertType;
    status: number;
    message: string;
    originalMessage: string;
    fieldValidationMessages: string[];
}
export enum AlertType {
    success,
    error,
    info
}