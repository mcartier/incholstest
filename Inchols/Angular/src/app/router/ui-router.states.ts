﻿//import { Hello } from "./components/hello.js";
//import { About } from "./components/about.js";
//import { People } from "./components/people.js";
//import { Person } from "./components/person.js";
import { AppComponent } from '../components/app.component';
import { WINDOW_PROVIDERS } from '../services/browser.window.provider';
import { StudentComponent } from '../components/students/student/student.component';
import { StudentsComponent } from '../components/students/students/students.component';
import { StudentListItemComponent } from '../components/students/student-list-item/student-list-item.component';
import { EditStudentComponent } from '../components/students/edit-student/edit-student.component';
import { ViewStudentComponent } from '../components/students/view-student/view-student.component';
import { LoginComponent } from '../components/login/login.component';
import { RegisterComponent } from '../components/register/register.component';
import { HomeComponent } from '../components/home/home.component';
import { StudentsContainerComponent } from '../components/students/students-container/students-container.component';
import { StudentService } from "../services/student.service.js";
import { Transition } from "@uirouter/angular";
import { Student } from '../models/student';
/** States */
export const rootState = {
    name: 'root',
    redirectTo: 'home',
    url: '',
    component:AppComponent
};


/**
 * This is the login state.  It is activated when the user navigates to /login, or if a unauthenticated
 * user attempts to access a protected state (or substate) which requires authentication. (see routerhooks/requiresAuth.js)
 *
 * It shows a fake login dialog and prompts the user to authenticate.  Once the user authenticates, it then
 * reactivates the state that the user originally came from.
 */
export const loginState = {
    name: 'login',
    url: '/login',
    component: LoginComponent,
    resolve: [
        { token: 'returnTo', deps: [Transition], resolveFn: returnTo },
    ]
};

export const registerState = {
    name: 'register',
    url: '/register',
    component: RegisterComponent,
    resolve: [
        { token: 'returnTo', deps: [Transition], resolveFn: returnTo },
    ]
};


/**
 * A resolve function for 'login' state which figures out what state to return to, after a successful login.
 *
 * If the user was initially redirected to login state (due to the requiresAuth redirect), then return the toState/params
 * they were redirected from.  Otherwise, if they transitioned directly, return the fromState/params.  Otherwise
 * return the main "home" state.
 */
export function returnTo($transition$: Transition): any {
    if ($transition$.redirectedFrom() != null) {
        // The user was redirected to the login state (e.g., via the requiresAuth hook when trying to activate contacts)
        // Return to the original attempted target state (e.g., contacts)
        return $transition$.redirectedFrom().targetState();
    }

    const $state = $transition$.router.stateService;

    // The user was not redirected to the login state; they directly activated the login state somehow.
    // Return them to the state they came from.
    if ($transition$.from().name !== '') {
        return $state.target($transition$.from(), $transition$.params('from'));
    }

    // If the fromState's name is empty, then this was the initial transition. Just return them to the home state
    return $state.target('home');
}

export const homeState = {
     name: "home",
    url: '/home',
    component: HomeComponent

};
export const studentsContainerState = {
    name: "studentContainer",
    component: StudentsContainerComponent,
    onEnter: function ($transition$: Transition, $state$) {
        const studentService: StudentService = $transition$.injector().get(StudentService);
        studentService.LoadStudentList(0);
    }


};
export const studentsState = {
    parent: "studentContainer",
    name: "students",
    url: '/students',
    views: {
        "left@studentContainer": { component: StudentsComponent }
    },
    //component: StudentsComponent,
    data: { requiresAuth: true }
    

};
export const studentState = {
    parent: "studentContainer",
    name: "viewstudent",
    views: {
        "left@studentContainer": { component: StudentsComponent },
        "right@studentContainer": { component: StudentComponent }
    },
    //component: StudentComponent,
    url: '/students/:studentId',
    data: { requiresAuth: true },



};
export const editStudentState = {
    parent: "studentContainer",
    url: '/students/:studentId/edit',
    name: "editstudent",
    views: {
        "left@studentContainer": { component: StudentsComponent },
        "right@studentContainer": { component: EditStudentComponent }
    },
    //component: EditStudentComponent,
    data: { requiresAuth: true },
    resolve: [
        { token: 'returnTo', deps: [Transition], resolveFn: returnTo },
    ]

};


export const newStudentState = {
    parent: "studentContainer",
    url: '/students/new',
    name: "newstudent",
    views: {
        "left@studentContainer": { component: StudentsComponent },
        "right@studentContainer": { component: EditStudentComponent }
    },
    //component: EditStudentComponent,
    data: { requiresAuth: true },
    resolve: [
        { token: 'returnTo', deps: [Transition], resolveFn: returnTo },
    ]


};

// This future state is a placeholder for all the lazy loaded Contacts states
// The Contacts NgModule isn't loaded until a Contacts link is activated
export const contactsFutureState = {
    name: 'contacts.**',
    url: '/contacts',
    loadChildren: './contacts/contacts.module#ContactsModule'
};

// This future state is a placeholder for the lazy loaded Prefs states
export const prefsFutureState = {
    name: 'prefs.**',
    url: '/prefs',
    loadChildren: './prefs/prefs.module#PrefsModule'
};

// This future state is a placeholder for the lazy loaded My Messages feature module
export const mymessagesFutureState = {
    name: 'mymessages.**',
    url: '/mymessages',
    loadChildren: './mymessages/mymessages.module#MymessagesModule'
};


export const APP_STATES = [
    rootState,
    homeState,
    registerState,
    loginState,
    studentsContainerState,
    studentsState,
    studentState,
    editStudentState,
    newStudentState
];

