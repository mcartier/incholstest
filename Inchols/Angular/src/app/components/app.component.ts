import { Component } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    constructor(private authenticationService: AuthenticationService) { }
    private isLogedIn: boolean = false;
    //this.authenticationService.LogedInStateObs
    ngOnInit() {
        this.authenticationService.LogedInStateObs.subscribe((isLogedIn: boolean) => {
            this.isLogedIn = isLogedIn
        });
    }
    
    title = 'angular-inchols-test';
    logout() {
        this.authenticationService.logout();
    }
}
