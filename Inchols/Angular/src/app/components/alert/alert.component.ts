import { Component, OnInit, OnDestroy } from '@angular/core';
import { AlertService } from "../../services/alert.service.js";
import { Subscription } from 'rxjs';
import { IAlert, AlertType} from '../../models/alertInterface'
@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit,OnDestroy {

  constructor(private alertService:AlertService) { }
    private subscription: Subscription;
    private alerts: IAlert[] = [];
   // alert: any;
    ngOnInit() {
        this.subscription = this.alertService.getAlerts().subscribe(alert => {
          if (alert) {
                this.alerts.push(alert);
                setTimeout(function (clearalert, alert, alerts) {
                    clearalert(alert, alerts);
                }, 1500, this.clearalert, alert, this.alerts);
               
            }
             //this.alert = alert;
        });
    }
    clearalert(alert: any, alerts: any[]) {
        let ind: number = -1;
        alerts.forEach((msg, index) => {
             if (msg == alert) {
                ind = index;
            }
            else if (msg === alert) {
                ind = index;
            }
            
        })
         if (ind > -1) { alerts.splice(ind,1); }
    }
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}
