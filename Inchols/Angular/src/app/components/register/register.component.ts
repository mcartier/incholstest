import { Component, OnInit, Input  } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { TargetState, StateService } from '@uirouter/core';
import { AuthenticationService } from '../../services/authentication.service';
import { RxwebValidators, RxFormBuilder } from "@rxweb/reactive-form-validators";
import { AlertService } from "../../services/alert.service.js";
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
    @Input() returnTo: TargetState;
    registerForm: FormGroup;
    submitted = false;
    submitting: boolean;
    errorMessage: string;

    constructor(private alertService:AlertService,private authenticationService: AuthenticationService,
        private $state: StateService, private formBuilder: RxFormBuilder
    ) { }

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            username: ['', Validators.required],
            password: ['', [Validators.required, Validators.minLength(6)]],
            confirmpassword: ['', RxwebValidators.compare({ fieldName: 'password' })]
        })
    }
    get f() { return this.registerForm.controls; }
    checkPasswords(group: FormGroup) { // here we have the 'passwords' group
        let pass = group.controls.password.value;
        let confirmPass = group.controls.confirmPass.value;

        return pass === confirmPass ? null : { notSame: true }
    }
    returnToOriginalState = () => {
        const state = this.returnTo.state();
        const params = this.returnTo.params();
        const options = Object.assign({}, this.returnTo.options(), { reload: true });
        this.$state.go(state, params, options);
    };
   // matcher = new PasswordComfirmationErrorStateMatcher();
    onSubmit() {
        this.submitted = true;
        this.submitting = true;

        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }

        
        const showError = (errorMessage) =>
            this.errorMessage = errorMessage;

        const stop = () => this.submitting = false;
        this.authenticationService.register(this.registerForm.value.username, this.registerForm.value.password, this.registerForm.value.confirmpassword, this.registerForm.value.email)
            .then(this.returnToOriginalState)
            .catch(showError)
            .then(stop, stop);
        
    }

}
