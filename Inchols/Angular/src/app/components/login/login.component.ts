import { Component, OnInit, Input } from '@angular/core';
import { TargetState, StateService } from '@uirouter/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '../../services/authentication.service';
import { RxwebValidators, RxFormBuilder } from "@rxweb/reactive-form-validators";
import { AlertService } from "../../services/alert.service.js";
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    @Input() returnTo: TargetState;
    loginForm: FormGroup;
    submitted = false;
    credentials = { username: null, password: null };
    submitting: boolean;
    errorMessage: string;

    constructor(private alertService:AlertService,private formBuilder: RxFormBuilder, private authenticationService: AuthenticationService,
        private $state: StateService
    ) {
       
        this.credentials = {
            username: "bestuser",
            password: "Password!"
        };
    }
    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        })
    }
    get f() { return this.loginForm.controls; }
    returnToOriginalState = () => {
        const state = this.returnTo.state();
        const params = this.returnTo.params();
        const options = Object.assign({}, this.returnTo.options(), { reload: true });
        this.$state.go(state, params, options);
    };
    onSubmit() {
        this.submitted = true;
        
        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }
        this.submitting = true;

        
        const showError = (errorMessage) =>
            this.errorMessage = errorMessage;

        const stop = () => this.submitting = false;
        this.authenticationService.login(this.loginForm.value.username, this.loginForm.value.password)
            .then(this.returnToOriginalState)
            .catch(showError)
            .then(stop, stop);

    }
    
}
