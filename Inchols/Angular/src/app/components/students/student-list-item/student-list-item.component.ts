import { Component, OnInit, Input   } from '@angular/core';
import { Student } from '../../../models/student';
import { StudentService } from "../../../services/student.service.js";
import { Subscription } from "rxjs";
import { AlertService } from "../../../services/alert.service.js";
import { StudentUIStateService } from "../../../services/student-uistate.service.js";
@Component({
  selector: 'app-student-list-item',
  templateUrl: './student-list-item.component.html',
  styleUrls: ['./student-list-item.component.css']
})
export class StudentListItemComponent implements OnInit {

    constructor(private studentUIStateService: StudentUIStateService,private alertService:AlertService,private studentService: StudentService) { }

    @Input() student: Student;
    isInEdit: boolean = false;

    ngOnInit() {
        this.studentUIStateService.StudentIsInEditModeObs.subscribe((s) => {
            this.isInEdit = s;
        })
  }
    deleteStudent() {
        if (this.student != null && this.student.id != 0) {
            this.studentService.removeStudentById(this.student.id, true) 
        }
        
    };

}
