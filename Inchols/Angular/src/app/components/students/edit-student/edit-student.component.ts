import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { TargetState, StateService } from '@uirouter/core';
import { RxwebValidators, RxFormBuilder } from "@rxweb/reactive-form-validators"
import { UIROUTER_DIRECTIVES } from '@uirouter/angular';
import { Transition } from "@uirouter/angular"
import { UIRouter } from '@uirouter/angular';
import { Student } from '../../../models/student';
import { StudentService } from "../../../services/student.service.js";
import { Subscription } from "rxjs";
import { AlertService } from "../../../services/alert.service.js";
@Component({
  selector: 'app-edit-student',
  templateUrl: './edit-student.component.html',
  styleUrls: ['./edit-student.component.css']
})
export class EditStudentComponent implements OnInit {

    constructor(private alertService:AlertService,private studentService: StudentService,
        private $state: StateService, private formBuilder: RxFormBuilder, private trans: Transition) { }
    @Input() returnTo: TargetState;
    editStudentForm: FormGroup;
    submitted = false;
    submitting: boolean;
    errorMessage: string;
    
    student: Student = new Student();
    
    studentId: number;
    subscription: Subscription;
    
    ngOnInit() {
        this.editStudentForm = this.formBuilder.group({
            firstName: ['', Validators.required],
            lastName: ['', Validators.required]
        })
        if (this.trans.params().studentId) {
            this.studentId = parseInt(this.trans.params().studentId);
            this.loadStudent();

        }
        
    }
    ngOnDestroy() {
        
    }
    get f() { return this.editStudentForm.controls; }

    onSubmit() {
        this.submitted = true;
        if (this.editStudentForm.invalid) {
            return;
        }
        this.student.firstName = this.editStudentForm.value.firstName;
        this.student.lastName = this.editStudentForm.value.lastName;
        if (this.student.id != null && this.student.id != 0) {
            this.studentService.saveStudent(this.student, true).subscribe(s => {
                setTimeout(function (returnToOriginalState) {
                    returnToOriginalState();
                }, 200, this.returnToOriginalState);
                //this.returnToOriginalState();
            });
        }
        else {
            this.studentService.createStudent(this.student, true).subscribe(s => {
                setTimeout(function (returnToOriginalState) {
                     returnToOriginalState();
                }, 200, this.returnToOriginalState);
                //this.returnToOriginalState();
            });
        }
    }
    returnToOriginalState = () => {
        const state = this.returnTo.state();
        const params = this.returnTo.params();
        const options = Object.assign({}, this.returnTo.options(), { reload: true });
        this.$state.go(state, params, options);
    };

    loadStudent(): void {
       this.studentService.getStudent(this.studentId).subscribe((s) => {
           this.student = s;
            this.editStudentForm.setValue({
               firstName : this.student.firstName,
               lastName : this.student.lastName,

           })
          
        });

    }
    cancelEdit() {
        this.returnToOriginalState();
    }
    deleteStudent() {
        if (this.student != null && this.student.id != 0) {
            this.studentService.removeStudentById(this.student.id, true)
        }

    };
   
}
