import { Component, OnInit } from '@angular/core';
import { Student } from '../../../models/student';
import { AlertService } from "../../../services/alert.service.js";
@Component({
  selector: 'app-view-student',
  templateUrl: './view-student.component.html',
  styleUrls: ['./view-student.component.css']
})
export class ViewStudentComponent implements OnInit {

  constructor(private alertService:AlertService) { }

  ngOnInit() {
  }

}
