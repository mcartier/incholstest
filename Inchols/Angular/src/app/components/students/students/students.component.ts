import { Component, OnInit } from '@angular/core';
import { Student } from '../../../models/student';
import { StudentService } from "../../../services/student.service.js";
import { Subscription } from "rxjs";
import { StudentUIStateService } from "../../../services/student-uistate.service.js";
import { AlertService } from "../../../services/alert.service.js";
@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

    constructor(private studentUIStateService: StudentUIStateService, private alertService: AlertService,private studentService: StudentService) { }
    isInEdit: boolean = false;
    students: Student[];
    subscription: Subscription;
    ngOnInit() {
        this.studentUIStateService.StudentIsInEditModeObs.subscribe((s) => {
             this.isInEdit = s;
        })
        this.subsricbe();
        //this.loadStudents();
    }
    ngOnDestroy() {
        this.unSubscribe();
    }
    addNewStudent() {
        this.studentService.SubscribeToStudentObs
        /*this.studentService.addStudent({
            createdDate: new Date,
            firstName: "lkjlkj",
            lastName: "ieroiwe",
            id: 0
        })*/
    }
    erase(id:number) {
        this.studentService.SubscribeToStudentObs(id).subscribe(res => {
            return res;
        }
            
    )
        this.studentService.LoadStudent(id);
       //this.studentService.removeStudentById(id);
    }
    subsricbe(): void {
        this.subscription =  this.studentService.studentsObs.subscribe(studs => this.students = studs);
    }
    unSubscribe(): void {
        this.subscription.unsubscribe();
    }
    loadStudents(): void {
        this.studentService.LoadStudentList(0);
        
    }
}
