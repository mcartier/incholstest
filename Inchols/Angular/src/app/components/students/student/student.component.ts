import { Component, OnInit } from '@angular/core';
import { UIROUTER_DIRECTIVES } from '@uirouter/angular';
import { Transition } from "@uirouter/angular"
import { UIRouter } from '@uirouter/angular';
import { Student } from '../../../models/student';
import { StudentService } from "../../../services/student.service.js";
import { Subscription } from "rxjs";
import { AlertService } from "../../../services/alert.service.js";
@Component({
    selector: 'app-student',
    templateUrl: './student.component.html',
    styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {

    constructor(private alertService:AlertService,private studentService: StudentService, private trans: Transition, private uiRouter: UIRouter) { }

    student: Student;
    studentId: number;
    subscription: Subscription;
    ngOnInit() {
        this.studentId = parseInt(this.trans.params().studentId);
        this.subsricbe();
        this.loadStudents();
    }
    ngOnDestroy() {
        this.unSubscribe();
    }
    subsricbe(): void {
        this.subscription = this.studentService.SubscribeToStudentObs(this.studentId).subscribe(
            (s) => {
                
                this.student = s;
            },
            (error) => { },
            () => {
                
                this.uiRouter.stateService.go('students');
            }
        );
    }
    unSubscribe(): void {
        this.subscription.unsubscribe();
    }
    loadStudents(): void {
        this.studentService.LoadStudent(this.studentId);

    }
    deleteStudent() {
        if (this.student != null && this.student.id != 0) {
            this.studentService.removeStudentById(this.student.id, true)
        }

    };

}
