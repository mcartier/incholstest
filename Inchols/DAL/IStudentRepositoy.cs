﻿using Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public interface IStudentRepositoy:IDisposable
    {
        IQueryable<Student> Students();
        Task<IQueryable<Student>> StudentsAsync();
        Task<Student> FindStudentAsync(int id);
        Task<int> UpdateStudentAsync(Student student);
        Task<int> CreateStudentAsync(Student student);
        Task<int> DeleteStudentAsync(Student student);
       
        void SetEntityState(Student student, EntityState state);
    }
}
