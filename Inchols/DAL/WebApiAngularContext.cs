﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Validation;

namespace DAL
{
    public class WebApiAngularContext : DbContext, IWebApiAngularContext
    {

        
        public DbSet<Student> Students { get; set; }
        public WebApiAngularContext()
              : base("webapiangular")
        {
            Configure();
            ((IObjectContextAdapter)this).ObjectContext
            .ObjectMaterialized += (sender, args) =>
            {
                var entity = args.Entity as IObjectWithState;
                if (entity != null)
                {
                    entity.State = State.Unchanged;
                }
            };
        }
        private void Configure()
        {
            System.Data.Entity.Database.SetInitializer<WebApiAngularContext>(new WebApiAngularDBInitializer());
            this.Configuration.LazyLoadingEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;
            this.Configuration.AutoDetectChangesEnabled = true;
            this.Configuration.ValidateOnSaveEnabled = true;
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

          
        }
        public bool LogChangesDuringSave { get; set; }

        private void PrintPropertyValues(
          DbPropertyValues values,
          IEnumerable<string> propertiesToPrint,
          int indent = 1)
        {
            foreach (var propertyName in propertiesToPrint)
            {
                var value = values[propertyName];
                if (value is DbPropertyValues)
                {
                    Console.WriteLine(
                      "{0}- Complex Property: {1}",
                      string.Empty.PadLeft(indent),
                      propertyName);

                    var complexPropertyValues = (DbPropertyValues)value;
                    PrintPropertyValues(
                      complexPropertyValues,
                      complexPropertyValues.PropertyNames,
                      indent + 1);
                }
                else
                {
                    Console.WriteLine(
                      "{0}- {1}: {2}",
                      string.Empty.PadLeft(indent),
                      propertyName,
                      values[propertyName]);
                }
            }
        }

        private IEnumerable<string> GetKeyPropertyNames(object entity)
        {
            var objectContext = ((IObjectContextAdapter)this).ObjectContext;
            return objectContext
            .ObjectStateManager
            .GetObjectStateEntry(entity)
            .EntityKey
            .EntityKeyValues
            .Select(k => k.Key);
        }

        protected override DbEntityValidationResult ValidateEntity(
          DbEntityEntry entityEntry,
          IDictionary<object, object> items)
        {
            var _items = new Dictionary<object, object>();
            return base.ValidateEntity(entityEntry, _items);
        }

        private static void ApplyLoggingData(DbEntityEntry entityEntry)
        {
            var logger = entityEntry.Entity as Logger;
            if (logger == null) return;
            logger.UpdateModificationLogValues(entityEntry.State == EntityState.Added);
        }

        public override int SaveChanges()
        {
            var autoDetectChanges = Configuration.AutoDetectChangesEnabled;

            try
            {
                Configuration.AutoDetectChangesEnabled = false;
                ChangeTracker.DetectChanges();

                var errors = GetValidationErrors().ToList();
                if (errors.Any())
                {
                    throw new DbEntityValidationException("Validation errors found during save.", errors);
                }

                foreach (var entity in this.ChangeTracker
                  .Entries()
                  .Where(e => e.State == EntityState.Added || e.State == EntityState.Modified))
                {
                    ApplyLoggingData(entity);
                }
                ChangeTracker.DetectChanges();

                Configuration.ValidateOnSaveEnabled = false;
                return base.SaveChanges();
            }
            finally
            {
                Configuration.AutoDetectChangesEnabled = autoDetectChanges;
            }
        }

        
        protected override bool ShouldValidateEntity(DbEntityEntry entityEntry)
        {
            return base.ShouldValidateEntity(entityEntry)
              || (entityEntry.State == EntityState.Deleted
                  && entityEntry.Entity is Student);
        }

        public void SetModified(object entity)
        {
            Entry(entity).State = EntityState.Modified;
        }

        public void SetState(object entity, EntityState state)
        {
            Entry(entity).State = state;
        }
    }
}
