﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;

namespace DAL.Tests
{
    public class TestWebApiAngularContext : IWebApiAngularContext, IDisposable
    {
        public TestWebApiAngularContext()
        {
            this.Students = new TestStudentDbSet();
        }
        public DbSet<Student> Students { get; set; }

        public int SaveChangesCount { get; private set; }
        public int SaveChanges()
        {
            this.SaveChangesCount++;
            return 1;
        }

        public Task<int> SaveChangesAsync()
        {
            this.SaveChangesCount++;
            return Task.FromResult<int>(1);
        }
       

        public void SetModified(object entity)
        {
          //  throw new NotImplementedException();
        }
        public void SetState(object entity, EntityState state)
        {
           
        }
        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                  
                }


                disposedValue = true;
            }
        }



        public void Dispose()
        {
            Dispose(true);

        }

        public Task<IQueryable<Student>> StudentsAsync()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
