﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using System.Data.Entity.Infrastructure;

namespace DAL.Tests
{
    public class TestStudentRepository : IStudentRepositoy
    {
        IWebApiAngularContext _webApiAngularContext;
        public TestStudentRepository(IWebApiAngularContext webApiAngularContext)
        {
            _webApiAngularContext = webApiAngularContext;
        }
        public async Task<int> CreateStudentAsync(Student student)
        {
            _webApiAngularContext.Students.Add(student);
            return await _webApiAngularContext.SaveChangesAsync();
        }

        public async Task<int> DeleteStudentAsync(Student student)
        {
            _webApiAngularContext.Students.Remove(student);
            return await _webApiAngularContext.SaveChangesAsync();
        }

        public async Task<Student> FindStudentAsync(int id)
        {
            return await _webApiAngularContext.Students.FindAsync(id);
        }

        public IQueryable<Student> Students()
        {
            return _webApiAngularContext.Students;
        }

        public async Task<int> UpdateStudentAsync(Student student)
        {
            try
            {
                _webApiAngularContext.SetModified(student);
                return await _webApiAngularContext.SaveChangesAsync();

            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        public Task<IQueryable<Student>> StudentsAsync()
        {
            throw new NotImplementedException();
        }
        public void SetEntityState(Student student, EntityState state)
        {
            _webApiAngularContext.SetState(student, state);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~TestStudentRepository() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
