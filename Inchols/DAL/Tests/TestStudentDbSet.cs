﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Tests
{
    public class TestStudentDbSet : TestDbSet<Student>
    {
        public override Student Find(params object[] keyValues)
        {
            var id = (int)keyValues.Single();
            return this.SingleOrDefault(b => b.Id == id);
        }
        public override Task<Student> FindAsync(params object[] keyValues)
        {
            var id = (int)keyValues.Single();
            return Task.FromResult<Student>(this.SingleOrDefault(b => b.Id == id));
        }
    }
}