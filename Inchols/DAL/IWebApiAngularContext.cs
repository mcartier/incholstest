﻿using Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public interface IWebApiAngularContext
    {
        DbSet<Student> Students { get; }
        int SaveChanges();
        Task<int> SaveChangesAsync();
        void SetModified(object entity);
        void SetState(object entity, EntityState state);
        void Dispose();
    }
}
