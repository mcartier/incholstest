﻿using Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace DAL
{
    public class WebApiAngularDBInitializer : CreateDatabaseIfNotExists<WebApiAngularContext>
    {
        protected override void Seed(WebApiAngularContext context)
        {
            
            //load some students
            context.Students.Add(new Student { FirstName = "Mike", LastName = "McDonald", CreatedDate = DateTime.Now });
            context.Students.Add(new Student { FirstName = "Marc", LastName = "Rouin", CreatedDate = DateTime.Now });
            context.Students.Add(new Student { FirstName = "Tim", LastName = "Lepp", CreatedDate = DateTime.Now });
            context.Students.Add(new Student { FirstName = "Steen", LastName = "Jensen", CreatedDate = DateTime.Now });
            context.Students.Add(new Student { FirstName = "Alex", LastName = "Wood", CreatedDate = DateTime.Now });
            context.Students.Add(new Student { FirstName = "Therese", LastName = "Gibson", CreatedDate = DateTime.Now });
            context.Students.Add(new Student { FirstName = "Marina", LastName = "Julien", CreatedDate = DateTime.Now });
            context.Students.Add(new Student { FirstName = "Ray", LastName = "OBrian", CreatedDate = DateTime.Now });
            context.Students.Add(new Student { FirstName = "Brent", LastName = "Dumont", CreatedDate = DateTime.Now });
            context.Students.Add(new Student { FirstName = "Keegan", LastName = "Fortin", CreatedDate = DateTime.Now });


        }
    }
}
