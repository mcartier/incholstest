﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace DAL
{
    public class StudentRepositoy : IStudentRepositoy
    {
        IWebApiAngularContext _webApiAngularContext;
        public StudentRepositoy(IWebApiAngularContext webApiAngularContext)
        {
            _webApiAngularContext = webApiAngularContext;
        }
        public async Task<int> CreateStudentAsync(Student student)
        {
            _webApiAngularContext.Students.Add(student);
            return await _webApiAngularContext.SaveChangesAsync();
        }

        public async Task<int> DeleteStudentAsync(Student student)
        {
            _webApiAngularContext.Students.Remove(student);
            return await _webApiAngularContext.SaveChangesAsync();
        }

        public async Task<Student> FindStudentAsync(int id)
        {
            return await _webApiAngularContext.Students.FindAsync(id);
        }

        public IQueryable<Student> Students()
        {
            return _webApiAngularContext.Students;
        }

        public async Task<int> UpdateStudentAsync(Student student)
        {
            try
            {
                _webApiAngularContext.SetModified(student);
                return await _webApiAngularContext.SaveChangesAsync();

            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        public Task<IQueryable<Student>> StudentsAsync()
        {
            throw new NotImplementedException();
        }
        public void SetEntityState(Student student, EntityState state)
        {
            _webApiAngularContext.SetState(student,state);
        }
        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _webApiAngularContext.Dispose();
                }


                disposedValue = true;
            }
        }



        public void Dispose()
        {
            Dispose(true);

        }

        #endregion
    }
}
