﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using DAL;
using Model;
using Model.Enumerations;

namespace BLL
{
    public class StudentService : IStudentService, IDisposable
    {
        private IStudentRepositoy _studentRepository;
         public StudentService(IStudentRepositoy studentRepository)
        {
            _studentRepository = studentRepository;
        }
        public async Task<Student> GetStudent(int id)
        {
            return await _studentRepository.FindStudentAsync(id);
        }

        public async Task<List<Student>> GetStudents(OrderByFieldEnum orderByField)
        {
          return await GetStudents().OrderBy(s => (orderByField == OrderByFieldEnum.LastName ? s.LastName : s.FirstName)).ToListAsync(); ;
           
        }

        public IQueryable<Student> GetStudents()
        {

            return _studentRepository.Students();
        }
        public async Task<IQueryable<Student>> GetStudentsAsync()
        {
            return await _studentRepository.StudentsAsync();
        }

        public async Task<int> UpdateStudent(Student student)
        {
            if (StudentExists(student.Id))
            {
                try
                {
                    return await _studentRepository.UpdateStudentAsync(student);

                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }
            }
            else
            {
                throw new InvalidOperationException("student does not exist");
            }
        }

        public async Task<int> CreateStudent(Student student)
        {
            student.CreatedDate = DateTime.Now;
            return await _studentRepository.CreateStudentAsync(student);
        }
        public async Task<int> DeleteStudent(int id)
        {
            var student = await GetStudent(id);
            return await DeleteStudent(student);
        }


        public async Task<int> DeleteStudent(Student student)
        {
            if (StudentExists(student.Id))
            {
                return await _studentRepository.DeleteStudentAsync(student);
            }
            else
            {
                throw new InvalidOperationException("student does not exist");
            }
        }

       
        public bool StudentExists(int id)
        {
            return GetStudents().Any(s => s.Id == id);
            
        }

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _studentRepository.Dispose();
                }


                disposedValue = true;
            }
        }

       
 
        public void Dispose()
        {
            Dispose(true);
         
        }
        #endregion
    }
}