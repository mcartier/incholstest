﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using Model.Enumerations;
using DAL;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace BLL.Tests
{
    public class TestStudentService : IStudentService
    {
        private IStudentRepositoy _studentRepository;
        public TestStudentService(IStudentRepositoy studentRepository)
        {
            _studentRepository = studentRepository;
        }
        public async Task<Student> GetStudent(int id)
        {
            return await _studentRepository.FindStudentAsync(id);
        }

        public async Task<List<Student>> GetStudents(OrderByFieldEnum orderByField)
        {
            return await GetStudents().OrderBy(s => (orderByField == OrderByFieldEnum.LastName ? s.LastName : s.FirstName)).ToListAsync(); ;

        }

        public IQueryable<Student> GetStudents()
        {

            return _studentRepository.Students();
        }
        public async Task<IQueryable<Student>> GetStudentsAsync()
        {
            return await _studentRepository.StudentsAsync();
        }

        public async Task<int> UpdateStudent(Student student)
        {
            try
            {
                return await _studentRepository.UpdateStudentAsync(student);

            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        public async Task<int> CreateStudent(Student student)
        {
            student.CreatedDate = DateTime.Now;
            return await _studentRepository.CreateStudentAsync(student);
        }
        public async Task<int> DeleteStudent(int id)
        {
            var student = await GetStudent(id);
            return await DeleteStudent(student);
        }


        public async Task<int> DeleteStudent(Student student)
        {
            return await _studentRepository.DeleteStudentAsync(student);
        }


        public bool StudentExists(int id)
        {
            return GetStudents().Count(e => e.Id == id) > 0;
        }


        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~TestStudentService() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
