﻿using Model;
using Model.Enumerations;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//using System.Threading.Tasks;
namespace BLL
{
    public interface IStudentService: IDisposable
    {
        IQueryable<Student> GetStudents();
         Task<List<Student>> GetStudents(OrderByFieldEnum orderByField);
         Task<Student> GetStudent(int id);
         Task<int> UpdateStudent(Student student);
         Task<int> CreateStudent(Student student);
         Task<int> DeleteStudent(int id);
         Task<int> DeleteStudent(Student student);
        bool StudentExists(int id);

     

    }
}
