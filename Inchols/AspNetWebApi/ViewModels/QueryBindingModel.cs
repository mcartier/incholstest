﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AspNetWebApi.ViewModels
{
    public class DiplayOptionsModel
    {
        public DiplayOptionsModel()
        {
            OrderBy = OrderByOptions.firstName;
        }
        public OrderByOptions OrderBy { get; set; }

    }
}