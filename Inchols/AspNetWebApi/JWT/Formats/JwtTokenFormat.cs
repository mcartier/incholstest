﻿using AspNetWebApi.Entities;
using AspNetWebApi.JWT.Models;
using AspNetWebApi.JWT.Options;
using AspNetWebApi.Models;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler.Encoder;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
namespace AspNetWebApi.JWT.Formats
{
    public class JwtTokenFormat : ISecureDataFormat<AuthenticationTicket>
    {
        private const string IssuedAtClaimName = "iat";
        private const string ExpiryClaimName = "exp";
        private const string JwtIdClaimName = "jti";
        private static DateTime _epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        private const string AudiencePropertyKey = "audience";
        private const string UserPropertyKey = "userName";

        private readonly string _issuer = string.Empty;

        public JwtTokenFormat(string issuer)
        {
            _issuer = issuer;
      

        }

        /// <summary>
        /// Sign the JWT token
        /// </summary>
        /// <param name="data">The data the JWT token will contain</param>
        /// <returns>The JWT token</returns>
        public string Protect(AuthenticationTicket data)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }
            string audienceId = data.Properties.Dictionary.ContainsKey(AudiencePropertyKey) ? data.Properties.Dictionary[AudiencePropertyKey] : null;

            if (string.IsNullOrWhiteSpace(audienceId)) throw new InvalidOperationException("AuthenticationTicket.Properties does not include audience");

            Audience audience = AudienceStore.FindAudience(audienceId);
            if (audience == null)
            {
                throw new InvalidOperationException("token invalid audience identifier");
            }

            string symmetricKeyAsBase64 = audience.Base64Secret;

            var signingKey = new SymmetricSecurityKey(TextEncodings.Base64Url.Decode(symmetricKeyAsBase64));
            var signingCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256);
            var issued = data.Properties.IssuedUtc;
            var expires = data.Properties.ExpiresUtc;

            if (!issued.HasValue || !expires.HasValue)
            {
                return null;
            }

            var jwtSecurityToken = new JwtSecurityToken(_issuer, audience.ClientId, data.Identity.Claims, issued.Value.UtcDateTime, expires.Value.UtcDateTime, signingCredentials);
            var jwtSecurityTokenHandler = new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken);

            return jwtSecurityTokenHandler;
        }

        /// <summary>
        /// Validate the JWT token
        /// </summary>
        /// <param name="protectedText">The JWT Token</param>
        /// <returns>The user identity information</returns>
        public AuthenticationTicket Unprotect(string protectedText)
        {
            if (string.IsNullOrWhiteSpace(protectedText))
            {
                throw new ArgumentNullException("protectedText");
            }

            var handler = new JwtSecurityTokenHandler();
            var token = handler.ReadToken(protectedText) as JwtSecurityToken;

            if (token == null)
            {
                throw new ArgumentOutOfRangeException("protectedText", "Invalid JWT Token");
            }
            //validate audienceid
            string audienceId = token.Audiences.FirstOrDefault();

            if (string.IsNullOrWhiteSpace(audienceId))
            {
                throw new InvalidOperationException("token does not include audience");
            }
            var audiences = AudienceStore.AudiencesList.Values.Select(a => a.ClientId).ToArray();
            if (audiences.Count() == 0)
            {
                throw new InvalidOperationException("token invalid audience identifier");
            }
            Audience audience = AudienceStore.FindAudience(audienceId);
            if (audience == null)
            {
                throw new InvalidOperationException("token invalid audience identifier");
            }

            var validationParameters = new TokenValidationParameters {
                IssuerSigningKey = new SymmetricSecurityKey(TextEncodings.Base64Url.Decode(audience.Base64Secret)),
                //remove since we already validate above
                //ValidAudiences = new [],
                // turn off since we validate above
                ValidateAudience = false,
                ValidateIssuer = true,
                ValidIssuer = _issuer,
                ValidateLifetime = true,
                ValidateIssuerSigningKey = true };

            var tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken validatedToken = null;
            ClaimsPrincipal claimsPrincipal;
            try
            {
                 claimsPrincipal = tokenHandler.ValidateToken(protectedText, validationParameters, out validatedToken);

            }
            catch (SecurityTokenException ste)
            {
                 throw new InvalidOperationException("token is invalid or has been tempered with"); ;
            }
            var claimsIdentity = (ClaimsIdentity)claimsPrincipal.Identity;

            var authenticationExtra = new AuthenticationProperties(new Dictionary<string, string>());
            if (claimsIdentity.Claims.Any(c => c.Type == ExpiryClaimName))
            {
                string expiryClaim = (from c in claimsIdentity.Claims where c.Type == ExpiryClaimName select c.Value).Single();
                authenticationExtra.ExpiresUtc = _epoch.AddSeconds(Convert.ToInt64(expiryClaim, CultureInfo.InvariantCulture));
            }

            if (claimsIdentity.Claims.Any(c => c.Type == IssuedAtClaimName))
            {
                string issued = (from c in claimsIdentity.Claims where c.Type == IssuedAtClaimName select c.Value).Single();
                authenticationExtra.IssuedUtc = _epoch.AddSeconds(Convert.ToInt64(issued, CultureInfo.InvariantCulture));
            }

            var returnedIdentity = new ClaimsIdentity(claimsIdentity.Claims, "JWT");

            return new AuthenticationTicket(returnedIdentity, authenticationExtra);
        }
    }
}