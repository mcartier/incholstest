﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AspNetWebApi.JWT.Options
{
    public class JwtAuthorizationServerOptions
    {
        public bool AllowInsecureHttp { set; get; } = true;
        public string TokenEndpointPath { set; get; } = "/oauth2/token";
        public TimeSpan AccessTokenExpireTimeSpan { set; get; } = TimeSpan.FromMinutes(30);
    }
}