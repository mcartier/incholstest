﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AspNetWebApi.JWT.Options
{
    public class JwtTokenOptions
    {
        public string AudienceId { set; get; }
        public string Issuer { set; get; }
        public string Secret { set; get; }
        public JwtTokenOptions(string audienceId, string issuer, string secret)
        {
            AudienceId = audienceId;
            Issuer = issuer;
            Secret = secret;
        }
    }
}