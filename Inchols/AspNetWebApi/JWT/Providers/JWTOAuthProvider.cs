﻿using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AspNetWebApi.Models;
using AspNetWebApi.Entities;
using System.Collections.Concurrent;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using Microsoft.Owin.Security.DataHandler.Encoder;
using System.Security.Claims;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.Cookies;
using AspNetWebApi.JWT.Models;

namespace AspNetWebApi.JWT.Providers
{
    public class JWTOAuthProvider : OAuthAuthorizationServerProvider
    {

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            string clientId = string.Empty;
            string clientSecret = string.Empty;
            string symmetricKeyAsBase64 = string.Empty;
            if (!context.TryGetBasicCredentials(out clientId, out clientSecret))
            {
                context.TryGetFormCredentials(out clientId, out clientSecret);
            }

            if (context.ClientId == null)
            {
                context.SetError("invalid_clientId", "client_Id is not set");
                return Task.FromResult<object>(null);
            }

            var audience = AudienceStore.FindAudience(context.ClientId);

            context.Validated();
            return Task.FromResult<object>(null);
        }
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            var userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();
            ApplicationUser user = await userManager.FindAsync(context.UserName, context.Password);

            if (user == null)
            {
                context.SetError("invalid_grant", "The user name or password is incorrect");
                //context.Rejected();
                return ;
            }

            ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(userManager,
               OAuthDefaults.AuthenticationType);

         
            //If the username is the same as the password the user is validated :D

            var properties = new AuthenticationProperties(new Dictionary<string, string>
                {
                    {
                         "userName", (user.UserName == null) ? string.Empty : user.UserName
                    },
                {
                         "audience", (context.ClientId == null) ? string.Empty : context.ClientId
                    }
                });
            

            var ticket = new AuthenticationTicket(oAuthIdentity, properties);
            context.Validated(ticket);
            return ;
        }
        public static AuthenticationProperties CreateProperties(string userName, string audienceId)
        {
            IDictionary<string, string> data = new Dictionary<string, string>
            {
                { "userName", userName }, { "audienceid", audienceId }
            };
            return new AuthenticationProperties(data);
        }

    }
}