﻿using AspNetWebApi.JWT.Formats;
using AspNetWebApi.JWT.Options;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Owin;

namespace AspNetWebApi.JWT
{
    public static class JWTEasyStart
    {
        public static void UseJwtAuthorizationServer(this IAppBuilder app, IOAuthAuthorizationServerProvider oAuthProvider)
        {
            app.UseJwtAuthorizationServer(new JwtAuthorizationServerOptions(), oAuthProvider);
        }
        public static void UseJwtAuthorizationServer(this IAppBuilder app, JwtAuthorizationServerOptions easyJwtAuthorizationServerOptions, IOAuthAuthorizationServerProvider oAuthProvider)
        {
            app.UseOAuthAuthorizationServer(new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = easyJwtAuthorizationServerOptions.AllowInsecureHttp,
                TokenEndpointPath = new PathString(easyJwtAuthorizationServerOptions.TokenEndpointPath),
                AccessTokenExpireTimeSpan = easyJwtAuthorizationServerOptions.AccessTokenExpireTimeSpan,
                Provider = oAuthProvider,
                AuthenticationMode = AuthenticationMode.Active,
                AuthenticationType = "Bearer",
                AccessTokenFormat = new JwtTokenFormat("http://localhost/")
            });
        }
        public static void UseJwtAuthentication(this IAppBuilder app)
        {
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions
            {
                AccessTokenFormat = new JwtTokenFormat("http://localhost/"),
                AuthenticationMode = AuthenticationMode.Active,
                AuthenticationType = "Bearer",
                Description = new AuthenticationDescription()
            });
        }
    }
}