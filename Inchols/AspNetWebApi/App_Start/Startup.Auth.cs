﻿using Microsoft.Owin.Security.OAuth;
using Owin;
using AspNetWebApi.Models;
using Microsoft.Owin;
using AspNetWebApi.JWT.Providers;
using System;
using AspNetWebApi.JWT.Formats;
using AspNetWebApi.JWT.Options;
using AspNetWebApi.JWT;

namespace AspNetWebApi
{
    public partial class Startup
    {
        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }

          // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            app.CreatePerOwinContext(ApplicationDbContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);

            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            //app.UseCookieAuthentication(new CookieAuthenticationOptions
            // {
            //     AuthenticationType = CookieAuthenticationDefaults.AuthenticationType
            //  });
            //  app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);
            var easyJwtAuthorizationServerOptions = new JwtAuthorizationServerOptions();
            easyJwtAuthorizationServerOptions.AllowInsecureHttp = true;
            easyJwtAuthorizationServerOptions.TokenEndpointPath = "/oauth2/token";
            easyJwtAuthorizationServerOptions.AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(30);

           
            app.UseJwtAuthorizationServer(easyJwtAuthorizationServerOptions, new JWTOAuthProvider());
            app.UseJwtAuthentication();



        }
    }
}
