﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Microsoft.Owin.Security;
using AspNetWebApi.Models;
using Microsoft.Owin.Security.Cookies;
using Microsoft.AspNet.Identity;
using AspNetWebApi.JWT.Providers;
using System.Configuration;

[assembly: OwinStartup(typeof(AspNetWebApi.Startup))]

namespace AspNetWebApi
{
    public partial class Startup
    {
        public static string PublicClientId { get; private set; }

          public void Configuration(IAppBuilder app)
        {
            HttpConfiguration config = new HttpConfiguration();
            ConfigureAuth(app);

            // Web API routes
            config.MapHttpAttributeRoutes();
           
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

            app.UseWebApi(config);
        }
      
    }
}
