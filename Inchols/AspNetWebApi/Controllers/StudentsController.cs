﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Model;
using BLL;
using Model.Enumerations;
using System.Linq;

namespace AspNetWebApi.Controllers
{
    [Authorize]
    public class StudentsController : ApiController
    {
        private IStudentService _studentService;

        public StudentsController()
        {
            
            _studentService = StructureMap.ObjectFactory.GetInstance<StudentService>();
        }
        public StudentsController(IStudentService studentService)
        {
            _studentService = studentService;
        }
        // GET: api/Students

        public async Task<IHttpActionResult> GetStudents(int? orderBy)
        {
            if (orderBy == null)
            {
                return BadRequest("orderBy parameter is required");
            }
            var students = await _studentService.GetStudents(orderBy == (int)OrderByFieldEnum.LastName ? OrderByFieldEnum.LastName : OrderByFieldEnum.FirstName);
            //var students = await Task.FromResult<IQueryable<Student>>(_studentService.GetStudents());
            return Ok(students);
        }

        // GET: api/Students/5
        [ResponseType(typeof(Student))]
        public async Task<IHttpActionResult> GetStudent(int id)
        {
            Student student = await _studentService.GetStudent(id);
            if (student == null)
            {
                return NotFound();
            }

            return Ok(student);
        }

        // PUT: api/Students/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutStudent(int id, Student student)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != student.Id)
            {
                return BadRequest();
            }


            try
            {
                await _studentService.UpdateStudent(student);
            }
            catch (Exception x)
            {
                if (!_studentService.StudentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }


            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Students
        [ResponseType(typeof(Student))]
        public async Task<IHttpActionResult> PostStudent(Student student)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            await _studentService.CreateStudent(student);

            return CreatedAtRoute("DefaultApi", new { id = student.Id }, student);
        }

        // DELETE: api/Students/5
        [ResponseType(typeof(Student))]
        public async Task<IHttpActionResult> DeleteStudent(int id)
        {
            Student student = await _studentService.GetStudent(id);
            if (student == null)
            {
                return NotFound();
            }

            await _studentService.DeleteStudent(student);

            return Ok(student);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _studentService.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}