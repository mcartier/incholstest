﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AspNetWebApi.Models;
using AspNetWebApi.Entities;
using AspNetWebApi.JWT.Models;

namespace AspNetWebApi.Controllers
{
    [RoutePrefix("api/audience")]
    public class AudienceController : ApiController
    {
        [Route("")]
        public IHttpActionResult Post(AudienceModel audienceModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Audience newAudience = AudienceStore.AddAudience(audienceModel.Name);

            return Ok<Audience>(newAudience);

        }
    }
}
